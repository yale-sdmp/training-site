activate :aria_current
activate :autoprefixer
activate :directory_indexes
activate :sprockets

set :css_dir, "assets/stylesheets"
set :fonts_dir, "assets/fonts"
set :images_dir, "assets/images"
set :js_dir, "assets/javascripts"
set :markdown,
  autolink: true,
  fenced_code_blocks: true,
  footnotes: true,
  highlight: true,
  smartypants: true,
  strikethrough: true,
  tables: true,
  with_toc_data: true
set :markdown_engine, :redcarpet

page "/*.json", layout: false
page "/*.txt", layout: false
page "/*.xml", layout: false

configure :development do
  # Temporarily disabling for https://github.com/middleman/middleman/issues/2142
  # activate :livereload do |reload|
  #   reload.no_swf = true
  # end
end

configure :build do
  set :build_dir, 'public'
  set :http_prefix, '/training-site'
  # activate :relative_assets
end

configure :production do
  activate :asset_hash
  activate :gzip
  activate :minify_css
  activate :minify_html
  activate :minify_javascript
end
